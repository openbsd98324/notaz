# notaz


wine on PI 

It may allow to run Starcraft.


````
Running Starcraft on the pi 4 with Raspbian Buster is pretty straightforward. I didn't need to compile a custom Wine or kernel. The biggest hurdle was getting it to run fullscreen in the correct aspect ratio (on a widescreen monitor). Will detail that.

First things first: I copied over a previously installed Starcraft folder from Windows. Version 1.16.1 is required. From the Starcraft CD (or ISO), copy install.exe to your Starcraft folder and rename it StarCraft.mpq. From the Broodwar CD (or ISO) copy the install.exe to your Starcraft folder and rename to BroodWar.mpq. This does away with the CD check (this is officially supported by Blizzard: see patch notes at https://liquipedia.net/starcraft/Patche ... tch_1.15.2 . Note that the NoCD applies to 1.15.2 and up, we will be running 1.16.1)

From there install Wine.
Code: Select all

sudo apt-get install wine

from https://notaz.gp2x.de/misc/starec/ download libscr.tar.xz and extract the contents to your Starcraft directory. Per the directions in libscr.txt you can now run Starcraft by opening lxterminal (or whatever) and switching to your Starcraft directory, then running
Code: Select all

LD_LIBRARY_PATH=. setarch linux32 -L wine libscr_sa_arm.exe.so
That's it. Much easier not having to compile a tweaked Wine, and/or a 3G/1G kernel.

If you are using a 4:3 monitor, or are fine with your 4:3 game being stretched to fill a 16:9 monitor, you are done. If you have 16:9 monitor and prefer to keep the original aspect ratio, things get a little more involved. I ended up using xrandr. This script will display Starcraft fullscreen, centered and pillarboxed (black bars on the sides) to maintain the correct aspect ratio:
Code: Select all

#!/bin/bash
cd "$(dirname "$0")"
vga=`xrandr | grep -P " connected (primary )?\d+" | sed -e "s/\(\w\+\) .*/\1/"`
res=640x480 && off=107
python - << EOF&
import gtk
def create_window():
    window = gtk.Window()
    window.set_title('sc_bg')
    window.set_default_size(200, 200)
    window.connect('destroy', gtk.main_quit)
    color = gtk.gdk.color_parse(str('#000000'))
    window.modify_bg(gtk.STATE_NORMAL, color)
    window.set_decorated(False)
    window.show()
    window.move(-30, -30)
    window.resize(2590, 1470)
create_window()
gtk.main()
EOF
echo $! > /tmp/sc_bg.pid
sleep 0.3
xrandr --output $vga --mode $res --panning $res --transform 1.33333333,0,-$off,0,1,0,0,0,1
LD_LIBRARY_PATH=. setarch linux32 -L wine libscr_sa_arm.exe.so
xrandr --output $vga --auto --panning 0x0 --scale 1x1
if [[ -e /tmp/sc_bg.pid ]]; then   
    kill `cat /tmp/sc_bg.pid`    
    rm /tmp/sc_bg.pid 
fi
Explanation: xrandr --output $vga --mode 640x480 --panning 640x480 --transform 1.33333333,0,-107,0,1,0,0,0,1 places your fullscreen 640x480 window in the center of your monitor, pillarboxed. The right side unfortunately shows stuff from your desktop (wallpaper, open windows) rather than blacking things out like the left side does. So the script above uses python to create a fullscreen black window (the lines between the EOFs are python, see https://en.wikipedia.org/wiki/Here_document). It only needs to be large enough to cover the 854×480 of your fullscreen 480p resolution, but I make it large enough to cover my 2560x1440 monitor to hide some of the xrandr transitions. You can edit window.resize(width,height) to whatever you want, having it be at least 884x510. The PID of the python process that created it is saved for later. After a brief pause (.1 wasn't long enough, .2 was. .3 to be safe) xrandr then gives us our Starcraft resolution. Game is run, then when it exits, xrandr resets us to our old resolution, and the black background window is closed using the stored PID. If anyone has a better way to do this I'd love to hear it :)

Things run full speed, up to x16 playback in replays. Also in game, the Speed Settings menu has an Enable CPU throttle setting. Check it to prevent Starcraft from using 100% of a core when it doesn't need to.

Create a menu entry for it with Main Menu, preferences, Main Menu Editor. New Item with name Starcraft (or whatever) and for the command, browse to and select the script from before and use an image of your choice for the icon. Works seamlessly here!



Everything needed is in the first post. The first post has been edited, part of the original first post is below.

Originally I got this to work by running Starcraft in a new xsession. This method still works running it from a text tty, but after a reboot it stopped working from an existing xsession, aka the desktop. On a fresh raspbian install it worked once more, until after a reboot it stopped working again and I can't figure out why (it just crashes the desktop session). Left here for edification, and maybe someone can figure out what happened. And it still works from tty1 if you want

this script will display Starcraft fullscreen, centered and pillarboxed* (black bars on the sides) to maintain the correct aspect ratio:
Code: Select all

#!/bin/bash
cd "$(dirname "$0")"
vga=`xrandr | grep -P " connected (primary )?\d+" | sed -e "s/\(\w\+\) .*/\1/"`
res=640x480 && off=107
xrandr --output $vga --mode $res --panning $res --transform 1.33333333,0,-$off,0,1,0,0,0,1
LD_LIBRARY_PATH=. setarch linux32 -L wine libscr_sa_arm.exe.so
xrandr --output $vga --auto --panning 0x0 --scale 1x1
I called this script starcraft.sh (the name can be anything, as long as it is consistent with the below script)

*This works pretty well, but when I said there were black bars on both sides, that's not entirely true. The right side shows junk from your desktop and other windows. The mouse can't go there, and gameplay is not affected but it looks bad. There are a couple possible solutions. One is to change res=640x480 && off=107 to res=640x480 && off=214. This will display Starcraft fullscreen, in the correct aspect ratio, but moved to the far right of your 16:9 monitor, with a doublesize black space to the left, and nothing to the right. I didn't consider this a serious option.

I ended up launching Starcraft in a second Xsession. This can be done most easily from a TTY (ctrl-alt-f2, say), but with some kludging can be done from the currently running X session. I used the following script:
Code: Select all

#!/bin/bash
cd "$(dirname "$0")"
sudo xinit ./starcraft.sh -- :1 -xf86config xorg-game.config vt8
I called it Starcraft.sh (distinct from starcraft.sh).
It launches a new session, runs the starcraft.sh script from before as the only GUI program, does so on a separate display, uses a separate xorg config (without this, the session with my desktop liked to restart), and puts it on the 8th virtual terminal. Sudo is necessary as regular users do not have permission to launch a session on a different virtual terminal. This runs Starcraft fullscreen, and pillarboxed. You can switch to your desktop with ctrl-alt-f7, and back to Starcraft with ctrl-alt-f8. On exiting Starcraft, you will be returned to your desktop.
*EDIT* this doesn't work, just crashes my desktop. First post edited with new solution. It worked for a while, then stopped and I don't know why so meh. 

Things run basically full speed, though I notice a little hitching when fast forwarding replays. This can be mitigated. Run Wine's regedit, and ensure you have a key HKEY_CURRENT_USER\Software\Wine\Direct3D with a string value "DirectDrawRenderer"="gdi". See https://wiki.winehq.org/DirectDraw#Perf ... ottlenecks (seems to be slightly outdated, as I believe opengl is the default now, making necessary our intervention to change it back to gdi). With that, fast forwarding through replays at x16 goes without a hitch, and gameplay hitches that might have happened before will be minimized.
*EDIT* not necessary when running in the desktop xsession, as in the edited first post, due I presume to better opengl acceleration


````

