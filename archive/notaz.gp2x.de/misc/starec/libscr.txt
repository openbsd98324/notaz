ARM recompiled exe of StarCraft

requires: 1.16.1 version of the game data files
usage: LD_LIBRARY_PATH=. setarch linux32 -L wine libscr_sa_arm.exe.so

"setarch -L" is needed because the game (or more likely compiled-in m$ c++
runtime) relies on pointers to be below 0x80000000 (as they are on Windows).
Since 3.3 kernel ARM Linux uses so called topdown mapping and uses addresses
above 0x80000000 by default.

LD_LIBRARY_PATH=. is needed for dynamic loader to find standard.snp_arm,
which is an ELF shared object after recompilation.

Beware of GCC bug 67037!
https://gcc.gnu.org/bugzilla/show_bug.cgi?id=67037
Some distributions (like Debian jessie) are affected by it.
workaround:
$ cd src/wine/dlls/kernel32
$ touch volume.c 
$ sed -i -e 's/-O2/-Os/' Makefile
$ make

You may also want to disable wine's OpenGL blits and use gdi instead,
see DirectDrawRenderer in http://wiki.winehq.org/UsefulRegistryKeys
